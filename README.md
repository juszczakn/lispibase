# lispibase

Keep track of which DDL/SQL has been ran against your database.

## Usage

See `integration-tests.lisp` under `tests`, but the gist of it is:
```
(asdf:load-system "lispibase")
(defparameter *changesets*
  (list
   '((:name "Adding new table")
     (:author "nick")
     (:sql "create table asdf (a number)"))))
(defun main ()
  (lispibase-core:set-logging t)
  ;; :exec-fn and :query-fn are defined by the user, see docstring.
  (lispibase-core:init-setup :sqlite :exec-fn #'sql-exec-fn :query-fn #'sql-query-fn)
  (lispibase-core:try-run-changesets *changesets*))
```

## Running tests

```
(ql:quickload :lispibase)
(asdf:test-system :lispibase)
```
