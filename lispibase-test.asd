(asdf:defsystem #:lispibase-test
  :version "0.0.1"
  :licence "BSD"
  :description ""
  :author "nick.juszczak"
  :long-description
  ""
  :depends-on (:fiveam :md5 :sqlite :lispibase)
  :components
  ((:module
    "test"
    :serial t
    :components
    ((:file "util-tests")
     (:file "integration-tests")
     (:file "db-specific-tests"))))
  :perform (test-op (op system)
                    (uiop:symbol-call :lispibase-db-specific-tests '#:run-tests)
                    (uiop:symbol-call :lispibase-integration-tests '#:run-tests)
                    (uiop:symbol-call :lispibase-util-tests '#:run-tests)))
