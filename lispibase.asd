(asdf:defsystem :lispibase
  :version "0.0.1"
  :licence "BSD"
  :description ""
  :author "nick.juszczak"
  :long-description
  ""
  :depends-on (:md5 :cl-ppcre :split-sequence)
  :in-order-to ((test-op (test-op "lispibase-test")))
  :components
  ((:module
    "src"
    :serial t
    :components
    ((:module "db-specific"
              :serial t
              :components
              ((:file "sqlite")))
     (:module "core"
              :serial t
              :components
              ((:file "package")
               (:file "util")
               (:file "lispibase-core")
               (:file "sql-file")))))))
