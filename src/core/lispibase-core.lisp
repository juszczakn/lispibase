(in-package :lispibase-core)
;; types

(deftype supported-db () '(member :sqlite))

(deftype bootstrap-sql-key ()
  '(member
    :init-already-ran-sql
    :init-sql
    :check-already-ran-sql
    :insert-changelog-ran-sql
    :update-changelog-ran-sql))

(deftype precondition-on-result ()
  '(member
    :fail
    :continue
    :mark-ran))

(define-condition initialization-error (error)
  ((text :initarg :text :reader :text)))

(define-condition precondition-failed-error (error)
  ((text :initarg :text :reader :text)))

(define-condition changeset-md5sum-changed (error)
  ((text :initarg :text :reader :text)))

;; globals


(defvar *changelog-ran-status*
  '(:ran 1 :failed 2 :not-executed 3))


(defun changelog-ran-status (status)
  (getf *changelog-ran-status* status))


(defparameter *db-type* nil
  "Type of db we're running against, one of lispibase-core::db-type")

(defparameter *exec-fn* nil
  "Holds a closure for executing db ddl.
(lambda (sql &rest params))")

(defparameter *query-fn* nil
  "Holds a closure for executing sql queries.
(lambda (sql &rest params))")

(defparameter *logging* nil
  "whether or not we're logging while running queries.")


(defun set-logging (on-or-off)
  "t or nil, controls *logging*."
  (setf *logging* on-or-off))


(defun register-db-exec-fn (fn)
  "see *exec-fn*"
  (setf *exec-fn* fn))


(defun register-db-query-fn (fn)
  "see *query-fn*"
  (setf *query-fn* fn))


;; fn


(defun exec-sql (sql &rest params)
  (apply *exec-fn* sql params)
  t)


(defun query-sql (sql &rest params)
  (apply *query-fn* sql params))


(defun logger (&rest s)
  "Super-simple logging."
  (if *logging* (format t "lispibase-logger:~{~a~}~%" s)))


(defun get-db-specific-query (query-name)
  "Get db-specific queries, used by lispibase to bootstrap.
query-name should be one of lispibase-core::bootstrap-sql-key."
  (check-type query-name bootstrap-sql-key)
  (case *db-type*
    (:sqlite (getf lispibase-sqlite:*sql-table* query-name))
    (otherwise (error 'initiailzation-error :text "db-type not supported"))))


(defun init-setup (db-type &key exec-fn query-fn)
  "Bootstrap lispibase. This needs to be called first before any ddl can be ran.

db-type should be one of lispibase-core::db-type.
exec-fn should conform to (sql &rest params) -> any
query-fn should conform to (sql &rest params) -> list "
  (check-type db-type supported-db)
  (setf *db-type* db-type)
  (if exec-fn (register-db-exec-fn exec-fn))
  (if query-fn (register-db-query-fn query-fn))
  (if (not *exec-fn*)
      (error 'initialization-error :text "No exec-fn provided for running ddl against db"))
  (if (not *query-fn*)
      (error 'initialization-error :text "No query-fn provided for running queries against db"))

  (if (query-sql (get-db-specific-query :init-already-ran-sql))
      (logger "lispibase already setup for current database, skipping init...")
      (progn
        (logger "Running init sql...")
        (mapcar (lambda (s) (exec-sql s)) (get-db-specific-query :init-sql)))))


(defun changeset-already-ran (name author sql)
  "Checks changelog table to see whether changeset has already been ran.
 :check-already-ran-sql should return the status of the changeset, if any, from
 cl_db_status."
  (let* ((sql-md5sum (md5sum-str sql))
         (already-ran-query (get-db-specific-query :check-already-ran-sql))
         (res (first (query-sql already-ran-query name author))))
    (logger "Checking if " author ":" name ":" sql-md5sum " has been ran.")
    ;; check if md5 has changed and throw err
    (when res
      (destructuring-bind (value prev-md5sum) res
        (declare (ignore value))
        (when (not (equal prev-md5sum sql-md5sum))
          (error 'changeset-md5sum-changed :text
                 (str "SQL changed, md5sum no longer matches; Was " prev-md5sum
                      ", now " sql-md5sum ".")))))
    res))


(defun changeset-never-ran (name author sql)
  "See `changeset-already-ran`."
  (not (changeset-already-ran name author sql)))


(defun get-changeset-vals (changeset-alist)
  "Gets all the supported standard values for a changeset."
  (get-assoc-vals changeset-alist :name :author :sql :precondition :flags))


(defun check-precondition (precondition)
  "If there was a precondition and a result returned from sql,
 return the `precondition-on-result` keyword specificed.
 Else, just returns :continue."
  ;; '(:precondition ((:sql "select 1 from dual") (:on-result :fail)))
  (if precondition
      (destructuring-bind (sql on-result) (get-assoc-vals precondition :sql :on-result)
        (check-type on-result precondition-on-result)
        (if (query-sql sql)
            on-result
            :continue))
      :continue))


(defun upsert-changeset-ran (name author sql)
  "Upsert changeset as ran to changelog.
 Should already have checked by now if it _should_ have been ran."

  (let ((mark-ran (changelog-ran-status :ran))
        (sql-md5sum (md5sum-str sql))
        (insert-sql (get-db-specific-query :insert-changelog-ran-sql))
        (update-sql (get-db-specific-query :update-changelog-ran-sql)))
    (if (changeset-already-ran name author sql)
        (exec-sql update-sql sql-md5sum name author)
        (exec-sql insert-sql name author sql-md5sum mark-ran))))


(defun run-changeset (author name sql precondition)
  (let ((mark-ran (changelog-ran-status :ran))
        (sql-md5sum (md5sum-str sql))
        (precond-check (check-precondition precondition))
        (changelog-sql (get-db-specific-query :insert-changelog-ran-sql)))

    (if (equal :fail precond-check)
        (error 'precondition-failed-error :text (str "Precondition failed for " name)))

    (if (equal :mark-ran precond-check)
        (exec-sql changelog-sql name author sql-md5sum mark-ran))

    (if (equal :continue precond-check)
        (progn
          (logger "Running " author ":" name ":" sql-md5sum "...")
          (exec-sql sql)
          (upsert-changeset-ran name author sql)))))


(defun try-run-changeset (changeset-alist)
  "Tries to run the changeset."
  (destructuring-bind (name author sql precondition flags)
      (get-changeset-vals changeset-alist)
    (let ((always-run-sql (find :run-always flags)))
      ;;
      (if (or always-run-sql
              (changeset-never-ran name author sql))
          (run-changeset author name sql precondition)
          (progn
            (logger "Already ran, moving on."))))))


(defun try-run-changesets (changeset-alists)
  "Takes a tree of changeset alists and runs them against the configured database.

A changeset-alist is of the form:

  '(:name \"A description.\"
    :author \"My name\"
    :precondition ((:sql \"select 1 from...\") (:on-result :fail))
    :sql \"insert into...\"
    :flags (:run-always))

`try-run-changesets` can be passed any tree of arbitrarily nested changeset-alists.

`init-setup` must be ran prior to this."
  (let ((changesets (flatten-alists changeset-alists :name)))
    (mapcar #'try-run-changeset changesets)
    (logger "Done.")
    nil))
