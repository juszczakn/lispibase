(in-package :lispibase-core)


(defparameter *sql-file-changeset-delim* "^--\\\s*db-changeset\\\s*$"
  "Regex used to delimit changesets in a given .sql file.
This line needs to be the first line in a given changeset.

Default value is `-- db-changeset`, ignoring any spaces after
the `--` and after `db-changeset`.
")


(defun read-in-sql-file-lines (path)
  (with-open-file (stream path)
    (loop for line = (read-line stream nil)
       while line
       collect line)))


(defun partition-sql-by-delim-line (file-lines)
  (flet ((line-is-delim-p (line)
           (cl-ppcre:scan *sql-file-changeset-delim* line)))
    (split-sequence:split-sequence-if #'line-is-delim-p file-lines)))


(defun match-line (lines regex &aux (line-num 0))
  "Returns the match and the line number of the first match
of regex in the list of `lines` given."
  (labels ((match-name (line)
             (multiple-value-bind
                   (_ matches) (cl-ppcre:scan-to-strings regex line)
               (declare (ignore _))
               (when matches (clean-string (elt matches 0))))))
    (dolist (line lines)
      (let ((m (match-name line)))
        (if m
            (return (values m line-num))
            (progn (incf line-num) nil))))))


(defun match-line-only-line (lines regex)
  (multiple-value-bind (line _) (match-line lines regex)
    (declare (ignore _))
    line))


(defun match-name (lines)
  (match-line-only-line lines "^--\\\s*name:(.*)$"))


(defun match-author (lines)
  (match-line-only-line lines "^--\\\s*author:(.*)$"))


(defun match-sql (lines)
  "Collect every line in `lines` that follows the header comments
and create one sql string from them."
  (let ((non-comment-sql "^\\\s*([^-\\\s]+)"))
    (multiple-value-bind (_ line-num) (match-line lines non-comment-sql)
      (declare (ignore _))
      (flet ((append-newline (s) (str s #\newline)))
        (when line-num
          (let ((sql-lines (nthcdr line-num lines)))
            (clean-string (apply #'str (mapcar #'append-newline sql-lines)))))))))


(defun assemble-changeset (lines)
  "Takes multiple lines from a file that has been delimited
and assembles them into a valid changeset."
  (let* ((name (match-name lines))
         (author (match-author lines))
         (sql (match-sql lines))
         (valid-changeset name))
    (when valid-changeset
      (list
       (list :name name)
       (list :author author)
       (list :sql sql)))))


(defun read-file-to-changesets (path)
  "Read a .sql file to a list of changesets.
Required (and only currently supported) header comments are:
 - db-changeset (configurable via `*sql-file-changeset-delim*`)
 - name
 - author

Example sql file:

-- db-changeset
-- name: My changeset.
-- author: My name
create table a (n number)

-- db-changeset
-- name: My other changeset.
-- author: My name
create table b (n number)
"
  (let* ((file-lines (read-in-sql-file-lines path))
         (raw-changeset-sections (partition-sql-by-delim-line file-lines))
         (changesets (mapcar #'assemble-changeset raw-changeset-sections)))
    (remove-if-not #'identity changesets)))
