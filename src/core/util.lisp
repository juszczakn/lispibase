(in-package :lispibase-core)

;; utilities

(defun flatten (lst leaf-node-p &aux (result '()))
  "Flatten tree `lst`, whenever `leaf-node-p` is not true."
  (labels ((rflatten (lst1)
             (dolist (el lst1 result)
               ;; check if it's a leaf-node or not
               (if (not (funcall leaf-node-p el))
                   (rflatten el)
                   (push el result)))))
    (nreverse (rflatten lst))))

(defun flatten-alists (alists required-key)
  "Util function to flatten tree of alists.
Check if leaf is an alist using a required key
that should return a non-nil value from alist."
  (flatten alists (lambda (elt) (assoc required-key elt))))

(defun md5sum-str (s)
  "Util fn for getting md5 as plain hex."
  (let* ((hashed (md5sum-string s))
         (as-list (map 'list #'identity hashed))
         (md5-str (format nil "~{~x~}" as-list)))
    md5-str))

(defun get-assoc-val (key alist)
  (cadr (assoc key alist)))

(defun get-assoc-vals (alist &rest keys)
  (mapcar (lambda (key) (get-assoc-val key alist)) keys))

(defun str (&rest ss)
  (format nil "~{~a~}" ss))

(defun clean-string (s)
  (string-trim '(#\space #\tab #\newline) s))
