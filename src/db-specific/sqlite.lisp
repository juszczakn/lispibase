(in-package #:cl)

(defpackage #:lispibase-sqlite
  (:use #:cl)
  (:export #:*sql-table*))

(in-package #:lispibase-sqlite)

(defvar *init-already-ran-sql*
  "select 1 from sqlite_master where type='table' and name='cl_db_changelog'")

(defvar *init-sql*
  (list
   "
create table if not exists cl_db_status (
  cl_db_status_id integer primary key,
  value varchar(100) not null
)
"
   "
create table if not exists cl_db_changelog (
  cl_db_changelog_id integer primary key autoincrement,
  name varchar(1000) not null,
  author varchar(255) not null,
  md5sum varchar(32) not null,
  last_ran_date varchar(19) not null,
  cl_db_status_id integer not null,
  foreign key (cl_db_status_id) references cl_db_status (value),
  unique (name, author)
)
"
   "
insert into cl_db_status (cl_db_status_id, value)
values
(1, 'ran'),
(2, 'failed'),
(3, 'not-executed')
"))

(defvar *check-already-ran-sql*
  "
select
  cds.value,
  cdc.md5sum
from
  cl_db_status cds
join
  cl_db_changelog cdc on cds.cl_db_status_id = cdc.cl_db_status_id
where
  cdc.name = ?
  and cdc.author = ?
")

(defvar *insert-changelog-ran-sql*
  "
insert into cl_db_changelog (
  name, author, md5sum, last_ran_date, cl_db_status_id
)
values
(?, ?, ?, datetime(),  ?)
")

(defvar *update-changelog-ran-sql*
  "
update cl_db_changelog set
  last_ran_date = datetime(),
  md5sum = ?
where
  name = ?
  and author = ?
")

(defvar *sql-table*
  (list
   :init-already-ran-sql *init-already-ran-sql*
   :init-sql *init-sql*
   :check-already-ran-sql *check-already-ran-sql*
   :insert-changelog-ran-sql *insert-changelog-ran-sql*
   :update-changelog-ran-sql *update-changelog-ran-sql*))
