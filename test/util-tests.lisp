(in-package :cl)

(defpackage :lispibase-util-tests
  (:use :cl)
  (:export :run-tests))

(in-package :lispibase-util-tests)

(defvar *test-alist*
  '(((:a 1))
    (((((:a 42))
       ((:a 2)))))
    ((:a 4))
    ((:a 10))))

(defun flatten-alists-test ()
  (assert
   (equal '(((:a 1)) ((:a 42)) ((:a 2)) ((:a 4)) ((:a 10)))
          (lispibase-core::flatten-alists *test-alist* :a))))

(defun run-tests ()
  (flatten-alists-test))

;;(run-tests)
