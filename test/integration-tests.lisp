(in-package #:cl)
(defpackage #:lispibase-integration-tests
  (:use #:cl)
  (:export #:run-tests))
(in-package #:lispibase-integration-tests)

;; in-memory setup

(defparameter *connection* (sqlite:connect ":memory:"))
(defun sqlite-exec-fn (sql &rest params)
  (apply #'sqlite:execute-non-query *connection* sql params))
(defun sqlite-query-fn (sql &rest params)
  (apply #'sqlite:execute-to-list *connection* sql params))

;; some changes to test

(defparameter *other-changes*
  (list
   '((:name "creating table asdf2")
     (:author "nick")
     (:sql "create table asdf2 (a number)"))
   '((:name "insert into asdf2")
     (:author "nick")
     (:precondition ((:sql "select a from asdf2 where a = 1") (:on-result :continue)))
     (:sql "insert into asdf2 (a) values (1)")
     (:flags (:run-always)))
   '((:name "trying to insert into asdf2 again")
     (:author "nick")
     (:precondition ((:sql "select a from asdf2 where a = 1") (:on-result :mark-ran)))
     (:sql "insert into asdf2 (a) values (2)"))
   '((:name "creating table asdf3")
     (:author "nick")
     (:comment "check that we can reference `asdf` now")
     (:sql "create table asdf3 (a number, foreign key (a) references asdf (a))"))))

(defparameter *changes*
  (list
   '((:name "creating table asdf")
     (:author "nick")
     (:sql "create table asdf (a number)"))
   *other-changes*
   '((:name "creating table fdsa")
     (:author "nick")
     (:sql "create table fdsa (a number)"))))

;; tests

(defun run-setup ()
  (lispibase-core:init-setup :sqlite :exec-fn #'sqlite-exec-fn :query-fn #'sqlite-query-fn))

(defun run-changes ()
  (lispibase-core:try-run-changesets *changes*))

(defun some-inserts ()
  (mapcar (lambda (sql) (sqlite:execute-non-query *connection* sql))
          (list
           "insert into asdf (a) values (1)"
           "insert into fdsa (a) values (321)"
           "insert into asdf3 (a) values (321)")))

(assert (equal (list 1 2 3) (list 1 2 3)))

(defun test-new-tables-exist ()
  (assert
   (equal
    (list 1 321 321)
    (mapcar (lambda (sql) (sqlite:execute-single *connection* sql))
            (list
             "select a from asdf"
             "select a from fdsa"
             "select a from asdf3")))))

(defun test-runalways ()
  (assert
   (equal
    (list (list 1) (list 1))
    (sqlite:execute-to-list *connection* "select a from asdf2"))))

;; export

(defun run-tests ()
  (lispibase-core:set-logging t)
  ;; run twice to make sure not executing both times
  (run-setup)
  (run-setup)
  ;; run twice to make sure not executing both times
  (run-changes)
  (run-changes)
  ;; insert some test data
  (some-inserts)
  ;; make sure data is there
  (test-new-tables-exist)
  (test-runalways))


;;(run-tests)
