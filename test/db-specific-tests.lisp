(in-package #:cl)

(defpackage #:lispibase-db-specific-tests
  (:use #:cl)
  (:export #:run-tests))

(in-package #:lispibase-db-specific-tests)

;; util

(defun get-all-keys (plist)
  (mapcan (lambda (elt) (if (keywordp elt) (list elt))) plist))

;; tests

(defun sqlite-covers-all-keys ()
  (let ((all-sql-keys (get-all-keys lispibase-sqlite:*sql-table*)))
    (assert (equal 5 (length all-sql-keys)))
    (mapcar (lambda (entry) (check-type entry lispibase-core::bootstrap-sql-key)) all-sql-keys)
    nil))

;; export

(defun run-tests ()
  (sqlite-covers-all-keys))

;; (run-tests)
